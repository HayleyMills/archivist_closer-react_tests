#!/usr/bin/env python3

"""
    Delete an instrument
"""

import time
import sys
import logging
import os

from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import pandas as pd
import mylib


def delete_instrument(uname, pw):
    """
    Delete test instrument
    """
    driver = mylib.get_driver()
    ok = mylib.archivist_login(driver, uname, pw)
    if not ok:
        print("could not login!")
        return

    try:
        print("login success")

        url = os.path.join(mylib.main_url, 'admin', 'instruments')
        print(url)
        driver.get(url)
        time.sleep(5)

        # Find this newly added code list then delete it
        searchPrefix = driver.find_element(By.XPATH, '//input[@placeholder="Search by id (press return to perform search)"]')
        searchPrefix.send_keys(mylib.test_instrument)

        # locate id and link
        trs = driver.find_elements(by=By.XPATH, value="html/body/div/div/div/div/main/div/div/div/div/table/tbody/tr")

        print("This page has {} rows, searching for matching row".format(len(trs)))
        msg_dict = {}
        matching_idx = []
        for i in range(0, len(trs)):
            tr = trs[i]
            if mylib.test_instrument == tr.find_elements(By.XPATH, "td")[1].text:
                matching_idx.append(i)

        if len(matching_idx) == 0:
            print("Could not find a row matching the prefix")
        elif len(matching_idx) > 1:
            print("There was more than one row matching this prefix?")
        else:
            tr = trs[matching_idx[0]]

            # click on "Delete"
            deleteButton = tr.find_elements(By.XPATH, "td")[3].find_elements(By.TAG_NAME, 'button')[-1]
            deleteButton.click()

            # Confirm prefix
            confirmPrefix = driver.find_element(By.ID, "outlined-error-helper-text")
            confirmPrefix.send_keys(mylib.test_instrument)
            time.sleep(2)
            driver.find_element(By.XPATH, "/html/body/div[2]/div[3]/p[3]/button/span").click()


    finally:
        # logout no matter how the code ended
        mylib.archivist_logout(driver)
        driver.quit()


def main():

    uname = sys.argv[1]
    pw = sys.argv[2]

    delete_instrument(uname, pw)


if __name__ == "__main__":
    main()

